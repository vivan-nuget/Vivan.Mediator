﻿using FluentValidation.Results;
using MediatR;
using Vivan.Notifications.Entities;
using Vivan.Notifications.Interfaces;

namespace Vivan.Mediator.Commands
{
    public abstract class CommandHandler
    {
        protected readonly IMediator Mediator;
        protected readonly INotificationContext NotificationContext;

        protected CommandHandler(IMediator mediator, INotificationContext notificationContext)
        {
            Mediator = mediator;
            NotificationContext = notificationContext;
        }

        protected void AddNotification(string message) =>
            NotificationContext.AddNotification(new Notification(message));

        protected void AddNotification(string key, string message) =>
            NotificationContext.AddNotification(new Notification(key, message));

        protected void AddNotifications(ValidationResult validationResult) =>
            NotificationContext.AddNotifications(validationResult);

        public bool HasNotifications => NotificationContext.HasNotifications;
    }
}
