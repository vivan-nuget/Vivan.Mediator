﻿using FluentValidation;
using Vivan.Notifications.Validations;

namespace Vivan.Mediator.Commands
{
    public abstract class Command<TValidator> : Validatable where TValidator : IValidator
    {
        public override bool Validate() => UseValidator<TValidator>(this);
    }
}
